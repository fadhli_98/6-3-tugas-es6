// Soal 1
// buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang dengan arrow function lalu gunakan let atau const di dalam soal ini

const LuasPersegiPanjang = (panjang , lebar) => {
    return panjang * lebar
}

console.log(LuasPersegiPanjang(5 , 2));


// Jawaban Soal 1


//Soal 2

//Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana

// const newFunction = function literal(firstName, lastName){
//   return {
//     firstName: firstName,
//     lastName: lastName,
//     fullName: function(){
//       console.log(firstName + " " + lastName)
//     }
//   }
// }
 
//Driver Code 
// newFunction("William", "Imoh").fullName() 


const firstname = 'william'
const lastname = 'Imoh'

const fullname = `${firstname} ${lastname}`

console.log (fullname)

//Jawaban Soal 2

//Soal 3
//Diberikan sebuah objek sebagai berikut:
var newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
};
//   dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:
//   const firstName = newObject.firstName;
//   const lastName = newObject.lastName;
//   const address = newObject.address;
//   const hobby = newObject.hobby;
//   Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)
//   // Driver code
//   console.log(firstName, lastName, address, hobby)

const {firstName, lastName, address, hobby} = newObject

console.log(firstName, lastName, address, hobby)

//jawaban soal 3


//Soal 4

// Kombinasikan dua array berikut menggunakan array spreading ES6
let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = west.concat(east)


// //Driver Code
console.log(combined)

//jawaban soal 4

//soal 5
//sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:

const planet = "earth" 
const view = "glass" 

const before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit, ' + planet

console.log(before)


